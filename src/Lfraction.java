import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lfraction implements Comparable<Lfraction>, Cloneable {

   private long numerator;
   private long denominator;

   public static void main(String[] args) {
      System.out.println(valueOf("2/-4"));
      // System.out.println(valueOf("2/4/6"));
      // System.out.println(valueOf("2/4/"));
      // System.out.println(valueOf("2/x"));
   }

   public Lfraction (long numerator, long denominator) {
       if (denominator < 0) {
         denominator = denominator * -1;
         numerator = numerator * -1;
       }
       if (denominator == 0) {
         throw new ArithmeticException("Divide by zero");
       }
       this.numerator = numerator;
       this.denominator = denominator;
       reduceFraction();
   }

   public Lfraction (long a) {
      this.numerator = a;
      this.denominator = 1;
   }

   public long getLowestCommonMultiple(long a, long b) {

      // Source: Least Common Multiple, https://en.wikipedia.org/wiki/Least_common_multiple#Reduction_by_the_greatest_common_divisor

      a = Math.abs(a);
      b = Math.abs(b);

      if (a > b) {
         long tmp = a;
         a = b;
         b = tmp;
      }
      return Math.abs(a * b) / getGreatestCommonDivisor(a, b);
   }

   private long getGreatestCommonDivisor(long a, long b) {

      // Source: Eucleidan Algorithm, https://en.wikipedia.org/wiki/Euclidean_algorithm

      a = Math.abs(a);
      b = Math.abs(b);

      if (a > b) {
         long tmp = a;
         a = b;
         b = tmp;
      }

      while (b > 0) {
         long tmp = b;
         b = a % b;
         a = tmp;
      }
      return a;
   }

   private void reduceFraction(){
      long divisor = getGreatestCommonDivisor(getNumerator(), getDenominator());
      setNumerator(getNumerator() / divisor);
      setDenominator(getDenominator() / divisor);
   }

   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   @Override
   public boolean equals (Object m) {
      if (m instanceof Lfraction) {
         Lfraction secondFraction = (Lfraction) m;
         return compareTo(secondFraction) == 0;
      }
      return false;
   }

   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   public Lfraction plus (Lfraction m) {
      Lfraction firstFraction = this;
      Lfraction secondFraction = new Lfraction(m.getNumerator(), m.getDenominator());

      long lowestCommonMultiple = getLowestCommonMultiple(firstFraction.getDenominator(), secondFraction.getDenominator());
      long sumNumerator = firstFraction.getNumerator() * (lowestCommonMultiple / firstFraction.getDenominator()) +
              secondFraction.getNumerator() * (lowestCommonMultiple / secondFraction.getDenominator());
      return new Lfraction(sumNumerator, lowestCommonMultiple);
   }

   public Lfraction times (Lfraction m) {
      long sumNumerator = this. getNumerator() * m.getNumerator();
      long sumDenominator = this.getDenominator() * m.getDenominator();
      return new Lfraction(sumNumerator, sumDenominator);
   }

   public Lfraction inverse() {
      if (numerator == 0) {
         throw new ArithmeticException("Division by zero!");
      }
      return new Lfraction(getDenominator(), getNumerator());
   }

   public Lfraction opposite() {
      return new Lfraction(getNumerator() * - 1, getDenominator());
   }

   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0) {
         throw new ArithmeticException("Division by zero!");
      }
      return times(m.inverse());
   }

   @Override
   public int compareTo (Lfraction m) {

      // Brings two fractions to the same denominator

      Lfraction firstFraction = this;
      Lfraction secondFraction = new Lfraction(m.getNumerator(), m.getDenominator());

      long lowestCommonMultiple = getLowestCommonMultiple(firstFraction.getDenominator(),
              secondFraction.getDenominator());

      firstFraction = firstFraction.times(new Lfraction(lowestCommonMultiple / firstFraction.getDenominator()));
      secondFraction = secondFraction.times(new Lfraction(lowestCommonMultiple / secondFraction.getDenominator()));

      // Compares numerators

      if (firstFraction.numerator < secondFraction.numerator) {
         return -1;
      } else if (firstFraction.getNumerator() == secondFraction.getNumerator()
              && secondFraction.denominator == secondFraction.getDenominator()) {
         return 0;
      } else {
         return 1;
      }

   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      Lfraction cloned = (Lfraction) super.clone();
      cloned.setNumerator(this.getNumerator());
      cloned.setDenominator(this.getDenominator());
      return cloned;
   }

   public long integerPart() {
      return getNumerator() / getDenominator();
   }

   public Lfraction fractionPart() {
      long newNumerator = getNumerator() - (getDenominator() * integerPart());
      return new Lfraction(newNumerator, getDenominator());
   }

   public double toDouble() {
      return (double) getNumerator() / getDenominator();
   }

   public static Lfraction toLfraction (double fraction, long denominator) {
      long numerator = Math.round(fraction * denominator);
      return new Lfraction(numerator, denominator);
   }

   public static Lfraction valueOf (String s) {

      if (isInputStringValid(s)) {
         String[] parts = s.split("/");
         long numerator = Long.parseLong(parts[0].trim());
         long denominator = Long.parseLong(parts[1].trim());
         return new Lfraction(numerator, denominator);
      } else {
         String baseErrorMessage = String.format("Cannot convert String '%s' into Lfraction! ", s);
         String errorMessageDetails = getErrorMessageForInvalidString(s);
         throw new NumberFormatException(baseErrorMessage + errorMessageDetails);
      }
   }

   private static boolean isInputStringValid(String input){
      String regexString = "^[+-]?\\d+\\/[+-]?\\d+";
      Pattern pattern = Pattern.compile(regexString);
      Matcher matcher = pattern.matcher(input.trim());
      return matcher.matches();
   }

   private static String getErrorMessageForInvalidString(String input){
      if (input == null || input.equals("")) {
         return "String is empty!";
      }
      String separator = "/";
      String[] parts = input.split(separator);

      if (isExtraSeparatorAtEndOfInput(input)) {
         return String.format("Fraction cannot end with symbol '%s' ", separator);
      }
      if (parts.length > 2) {
         return String.format("Only one separator symbol '%s' allowed!", separator);
      } else if (! input.contains(separator)) {
         return String.format("No separator symbol '%s' detected!", separator);
      }
      if (parts.length == 1) {
         return "Missing denominator";
      }  else if (parts[0].equals("")) {
         return "Missing numerator!";
      } else if (! isInputANumber(parts[0])) {
         return String.format("Numerator '%s' is not a valid number!", parts[0]);
      }  else if (! isInputANumber(parts[1])) {
         return String.format("Denominator '%s' is not a valid number!", parts[1]);
      }
      throw new RuntimeException("No condition found where input string is a false Lfraction!");
   }

   private static boolean isExtraSeparatorAtEndOfInput(String input){
      return input.substring(input.length() - 1).equals("/");
   }

   private static boolean isInputANumber(String input){
      try{
         Long.parseLong(input);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   public long getNumerator() {
      return numerator;
   }

   public long getDenominator() {
      return denominator;
   }

   public void setDenominator(long denominator) {
      this.denominator = denominator;
   }

   public void setNumerator(long numerator) {
      this.numerator = numerator;
   }

}